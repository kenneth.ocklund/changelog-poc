# changelog-poc

This is a throwaway project to test the GitLab changelog generation.

## Idea

The idea is to use the GitLab CLI (glab) and use git trailers to generate a changelog.

The CLI uses the GitLab API, so it should be possible to do the same with curl in a build pipeline.

## Manual test

Prerequisites:
- Install glab (brew install glab)
- Log in to GitLab with glab, which requires an access token (glab auth login)

1. Overwrite the `CHANGELOG.md` file with commits including 1.0.0
```
glab changelog generate -v 1.0.0 --from 801a68d --to 6b22692 > CHANGELOG.md
```
2. Update the changelog with the next version (prepends existing changelog)
```
glab changelog generate -v 1.1.0 --from 6b22692 --to f0fbae3 > CHANGELOG.tmp;echo "" >> CHANGELOG.tmp;cat CHANGELOG.md >> CHANGELOG.tmp;mv CHANGELOG.tmp CHANGELOG.md
```
