## 1.1.0 (2024-05-22)

### Added (1 change)

- [Add changelog config](kenneth.ocklund/changelog-poc@f0fbae39d3170c56c3045c76d4e110b6d04581b3)

## 1.0.0 (2024-05-22)

### Added (1 change)

- [Add .gitignore file](kenneth.ocklund/changelog-poc@08e9f675b3b078f8747aad186995b0e76abc72a4)

### Changed (1 change)

- [Change README file](kenneth.ocklund/changelog-poc@6b2269278d3bbd67b99d7c96a84ce8e298bf50da)
